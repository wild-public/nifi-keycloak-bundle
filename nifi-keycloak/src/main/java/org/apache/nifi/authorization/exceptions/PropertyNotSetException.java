package org.apache.nifi.authorization.exceptions;

public class PropertyNotSetException extends RuntimeException {

    public PropertyNotSetException(String property) {
        super("Property '%s' was not set");
    }
}
